/*
 * Copyright (c) 2016-2016 ARM Limited. All rights reserved.
 * SPDX-License-Identifier: Apache-2.0
 * Licensed under the Apache License, Version 2.0 (the License); you may
 * not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an AS IS BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "at24mac.h"
#include "fsl_port.h"
#include "fsl_gpio.h"
#include "fsl_i2c.h"

/* Device addressing */
#define AT24MAC_EEPROM_ADDRESS		(0x0A<<4)
#define AT24MAC_RW_PROTECT_ADDRESS	(0x06<<4)
#define AT24MAC_SERIAL_ADDRESS		(0x0B<<4)

/* Known memory blocks */
#define AT24MAC_SERIAL_OFFSET	(0x80)
#define AT24MAC_EUI64_OFFSET	(0x98)
#define AT24MAC_EUI48_OFFSET	(0x9A)

#define SERIAL_LEN 16
#define EUI64_LEN 8
#define EUI48_LEN 6

#ifdef CPU_MK66FN2M0VMD18
#define SDA_PORT PORTC
#define SDA_PORT_CLK kCLOCK_PortC
#define SDA_PIN  10u
#define SDA_GPIO GPIOC

#define SCL_PORT PORTC
#define SCL_PIN  11u
#define SCL_PORT_CLK kCLOCK_PortC
#define SCL_GPIO GPIOC

#define I2C_ADDR  I2C1
#endif

static int i2c_read(I2C_Type* base, int address, char *data, int length, int stop);
static int i2c_write(I2C_Type* base, int address, const char *data, int length, int stop);

static void delay_loop(uint32_t count)
{
  __asm__ volatile (
    "%=:\n\t"
#if defined(__thumb__) && !defined(__thumb2__)
    "SUB  %0, #1\n\t"
#else
    "SUBS %0, %0, #1\n\t"
#endif
    "BCS  %=b\n\t"
    : "+l" (count)
    :
    : "cc"
  );
}

static void delay_us(uint32_t us)
{
  uint32_t cycles_per_us = SystemCoreClock / 1000000;
  // Cortex-M0 takes 4 cycles per loop (SUB=1, BCS=3)
  // Cortex-M3 and M4 takes 3 cycles per loop (SUB=1, BCS=2)
  // Cortex-M7 - who knows?
  // Cortex M3-M7 have "CYCCNT" - would be better than a software loop, but M0 doesn't
  // Assume 3 cycles per loop for now - will be 33% slow on M0. No biggie,
  // as original version of code was 300% slow on M4.
  // [Note that this very calculation, plus call overhead, will take multiple
  // cycles. Could well be 100ns on its own... So round down here, startup is
  // worth at least one loop iteration.]
  uint32_t count = (cycles_per_us * us) / 3;

  delay_loop(count);
}

AT24Mac::I2CReset::I2CReset()
{
    CLOCK_EnableClock(SDA_PORT_CLK);
    CLOCK_EnableClock(SCL_PORT_CLK);
    PORT_SetPinMux(SDA_PORT, SDA_PIN, kPORT_MuxAsGpio);
    PORT_SetPinMux(SCL_PORT, SCL_PIN, kPORT_MuxAsGpio);

    gpio_pin_config_t pin_config = {
        kGPIO_DigitalOutput, 1,
    };

    GPIO_PinInit(SDA_GPIO, SDA_PIN, &pin_config);

    pin_config.outputLogic = 0;
    GPIO_PinInit(SCL_GPIO, SCL_PIN, &pin_config);

    //generate 9 clocks for worst-case scenario
    for (int i = 0; i < 10; ++i) {

        GPIO_WritePinOutput(SCL_GPIO, SCL_PIN, 1);
        delay_us(5);
        GPIO_WritePinOutput(SCL_GPIO, SCL_PIN, 0);
        delay_us(5);
    }
    //generate a STOP condition
    GPIO_WritePinOutput(SDA_GPIO, SDA_PIN, 0);
    delay_us(5);
    GPIO_WritePinOutput(SCL_GPIO, SCL_PIN, 1);
    delay_us(5);
    GPIO_WritePinOutput(SDA_GPIO, SDA_PIN, 1);
    delay_us(5);
    PORT_SetPinMux(SDA_PORT, SDA_PIN, kPORT_PinDisabledOrAnalog);
    PORT_SetPinMux(SCL_PORT, SCL_PIN, kPORT_PinDisabledOrAnalog);
}

/*I2C needs to be reset before constructing the I2C object (in case I2C is stuck)
  because they use the same pins, therefore i2c_reset has to be before _i2c
  in the initializer list*/
AT24Mac::AT24Mac() : i2c_reset() 
{

    CLOCK_EnableClock(SDA_PORT_CLK);
    CLOCK_EnableClock(SCL_PORT_CLK);
    const port_pin_config_t pin_config = {
        kPORT_PullUp,                                            /* Internal pull-up resistor is enabled */
        kPORT_FastSlewRate,                                      /* Fast slew rate is configured */
        kPORT_PassiveFilterDisable,                              /* Passive filter is disabled */
        kPORT_OpenDrainEnable,                                   /* Open drain is enabled */
        kPORT_LowDriveStrength,                                  /* Low drive strength is configured */
        kPORT_MuxAlt2,                                           /* Pin is configured as I2C0_SDA */
        kPORT_UnlockRegister                                     /* Pin Control Register fields [15:0] are not locked */
    };    
    PORT_SetPinConfig(SDA_PORT, SDA_PIN, &pin_config);
    PORT_SetPinConfig(SCL_PORT, SCL_PIN, &pin_config);
    // Do nothing

    i2c_master_config_t masterConfig;
    I2C_MasterGetDefaultConfig(&masterConfig);
    I2C_MasterInit(I2C_ADDR, &masterConfig, CLOCK_GetFreq(kCLOCK_BusClk)); 
}

int AT24Mac::read_serial(void *buf)
{
    char offset = AT24MAC_SERIAL_OFFSET;
    if (i2c_write(I2C_ADDR, AT24MAC_SERIAL_ADDRESS, &offset, 1, 0) != 1)
        return -1; //No ACK
    return i2c_read(I2C_ADDR, AT24MAC_SERIAL_ADDRESS, (char*)buf, SERIAL_LEN, 1);
}

int AT24Mac::read_eui64(void *buf)
{
    char offset = AT24MAC_EUI64_OFFSET;
    if (i2c_write(I2C_ADDR, AT24MAC_SERIAL_ADDRESS, &offset, 1, 0) != 1)
        return -1; //No ACK
    return i2c_read(I2C_ADDR, AT24MAC_SERIAL_ADDRESS, (char*)buf, EUI64_LEN, 1);
}

int AT24Mac::read_eui48(void *buf)
{
    char offset = AT24MAC_EUI48_OFFSET;
    if (!i2c_write(I2C_ADDR, AT24MAC_SERIAL_ADDRESS, &offset, 1, 0) != 1)
        return -1; //No ACK
    return i2c_read(I2C_ADDR, AT24MAC_SERIAL_ADDRESS, (char*)buf, EUI48_LEN, 1);
}

static uint8_t next_repeated_start = 0;
static int i2c_read(I2C_Type* base, int address, char *data, int length, int stop)
{
    i2c_master_transfer_t master_xfer;

    memset(&master_xfer, 0, sizeof(master_xfer));
    master_xfer.slaveAddress = address >> 1;
    master_xfer.direction = kI2C_Read;
    master_xfer.data = (uint8_t *)data;
    master_xfer.dataSize = length;
    if (next_repeated_start) {
        master_xfer.flags |= kI2C_TransferRepeatedStartFlag;
    }
    if (!stop) {
        master_xfer.flags |= kI2C_TransferNoStopFlag;
    }
    next_repeated_start = master_xfer.flags & kI2C_TransferNoStopFlag ? 1 : 0;

    /* The below function will issue a STOP signal at the end of the transfer.
     * This is required by the hardware in order to receive the last byte
     */
    if (I2C_MasterTransferBlocking(base, &master_xfer) != kStatus_Success) {
        return -1;
    }

    return length;
}

static int i2c_write(I2C_Type* base, int address, const char *data, int length, int stop)
{
    i2c_master_transfer_t master_xfer;

    if (length == 0) {
        if (I2C_MasterStart(base, address >> 1, kI2C_Write) != kStatus_Success) {
            return -1;
        }

        while (!(base->S & kI2C_IntPendingFlag)) {
        }

        base->S = kI2C_IntPendingFlag;

        if (base->S & kI2C_ReceiveNakFlag) {
            I2C_MasterStop(base);
            return -1;
        } else {
            I2C_MasterStop(base);
            return length;
        }
    }

    memset(&master_xfer, 0, sizeof(master_xfer));
    master_xfer.slaveAddress = address >> 1;
    master_xfer.direction = kI2C_Write;
    master_xfer.data = (uint8_t *)data;
    master_xfer.dataSize = length;
    if (next_repeated_start) {
        master_xfer.flags |= kI2C_TransferRepeatedStartFlag;
    }
    if (!stop) {
        master_xfer.flags |= kI2C_TransferNoStopFlag;
    }
    next_repeated_start = master_xfer.flags & kI2C_TransferNoStopFlag ? 1 : 0;

    if (I2C_MasterTransferBlocking(base, &master_xfer) != kStatus_Success) {
        return -1;
    }

    return length;
}
