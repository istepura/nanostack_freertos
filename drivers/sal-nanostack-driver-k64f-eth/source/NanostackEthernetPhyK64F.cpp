#include <stdint.h>
#include "NanostackEthernetPhyK64F.h"
#include "k64f_eth_nanostack_port.h"
#include "FreeRTOS.h"
#include "semphr.h"
#include "fsl_sim.h"
#define CRC16
#include "crc.h"

static SemaphoreHandle_t enet_sem;

static int8_t iface_id = -1;
static bool iface_is_up = false;

extern "C" void mbed_mac_address(char *mac)
{
    uint16_t MAC[3];                        // 3 16 bits words for the MAC

    // get UID via SIM_UID macros defined in the K64F MCU CMSIS header file
    uint32_t UID[4];
    UID[0] = SIM->UIDH;
    UID[1] = SIM->UIDMH;
    UID[2] = SIM->UIDML;
    UID[3] = SIM->UIDL;

    // generate three CRC16's using different slices of the UUID
    MAC[0] = crcSlow((const uint8_t *)UID, 8);  // most significant half-word
    MAC[1] = crcSlow((const uint8_t *)UID, 12); 
    MAC[2] = crcSlow((const uint8_t *)UID, 16); // least significant half word

    // The network stack expects an array of 6 bytes
    // so we copy, and shift and copy from the half-word array to the byte array
    mac[0] = MAC[0] >> 8;
    mac[1] = MAC[0];
    mac[2] = MAC[1] >> 8;
    mac[3] = MAC[1];
    mac[4] = MAC[2] >> 8;
    mac[5] = MAC[2];

    // We want to force bits [1:0] of the most significant byte [0]
    // to be "10"
    // http://en.wikipedia.org/wiki/MAC_address

    mac[0] |= 0x02; // force bit 1 to a "1" = "Locally Administered"
    mac[0] &= 0xFE; // force bit 0 to a "0" = Unicast

}


static void driver_status_cb(uint8_t is_up, int8_t iface)
{
    BaseType_t xHigherPriorityTaskWoken = pdFALSE;
    iface_id = iface;
    iface_is_up = is_up?true:false;
    xSemaphoreGiveFromISR(enet_sem, &xHigherPriorityTaskWoken);
}

NanostackEthernetPhyK64F::NanostackEthernetPhyK64F()
{
    mbed_mac_address((char *)_mac);
    enet_sem = xSemaphoreCreateBinary();
}

int8_t NanostackEthernetPhyK64F::phy_register()
{
    arm_eth_phy_device_register(_mac, driver_status_cb);
    xSemaphoreTake(enet_sem, portMAX_DELAY);
    return iface_id;
}

void NanostackEthernetPhyK64F::get_mac_address(uint8_t *mac)
{
    memmove(mac, _mac, 6);
}

void NanostackEthernetPhyK64F::set_mac_address(uint8_t *mac)
{
    memmove(_mac, mac, 6);
}
