/*
 * Copyright (c) 2016 ARM Limited. All rights reserved.
 */

#include "rf_wrapper.h"
#include "mesh_system.h"
#include "nd_tasklet.h"
#include "NanostackRfPhyAtmel.h"
#include "FreeRTOS.h"
#include "task.h"
#include "mbed-trace/mbed_trace.h"
#include "drivers/eth_driver.h"
#include "nsdynmemLIB.h"
#include "border_router/borderrouter_tasklet.h"

#define TRACE_GROUP "wrapper"

NanostackRfPhyAtmel rf_phy;
static uint8_t mac[128];
static TaskHandle_t  gTaskHandle = NULL;

#define MBED_CONF_MBED_MESH_API_HEAP_SIZE 40000
static uint8_t app_stack_heap[MBED_CONF_MBED_MESH_API_HEAP_SIZE + 1];
static void mesh_system_heap_error_handler(heap_fail_t event);
extern "C" void ns_hal_init(void *heap, size_t h_size, void (*passed_fptr)(heap_fail_t), mem_stat_t *info_ptr);

static void mesh_callback(mesh_connection_status_t mesh_status)
{
    configASSERT(gTaskHandle);
    xTaskNotifyGive(gTaskHandle);
}

extern "C" int8_t start_mesh(TaskHandle_t taskHandle, StackRole_t role )
{
    gTaskHandle = taskHandle;
    ns_hal_init(app_stack_heap, MBED_CONF_MBED_MESH_API_HEAP_SIZE,
            mesh_system_heap_error_handler, NULL);
    if (role == StackRoleRouter)
    {
        mesh_system_init();
        uint8_t device_id = rf_phy.phy_register();
        rf_phy.get_mac_address(mac);
        nd_tasklet_init();
        uint8_t _network_interface_id = nd_tasklet_network_init(device_id);

        nd_tasklet_connect(&mesh_callback, _network_interface_id);
    }
    else {
        tr_info("FreeRTOS based BR is starting");
        border_router_tasklet_start();

    }
}

extern "C" int8_t rf_device_register()
{
    return rf_phy.rf_register();
}

extern "C" void rf_read_mac_address(uint8_t *mac)
{
    rf_phy.get_mac_address(mac);
}

extern "C" void backhaul_driver_init(void (*backhaul_driver_status_cb)(uint8_t, int8_t))
{
// Values allowed in "backhaul-driver" option
#define ETH 0
#define SLIP 1
#if MBED_CONF_APP_BACKHAUL_DRIVER == SLIP
    SlipMACDriver *pslipmacdriver;
    int8_t slipdrv_id = -1;
#if defined(MBED_CONF_APP_SLIP_HW_FLOW_CONTROL)
    pslipmacdriver = new SlipMACDriver(SERIAL_TX, SERIAL_RX, SERIAL_RTS, SERIAL_CTS);
#else
    pslipmacdriver = new SlipMACDriver(SERIAL_TX, SERIAL_RX);
#endif

    if (pslipmacdriver == NULL) {
        tr_error("Unable to create SLIP driver");
        return;
    }

    tr_info("Using SLIP backhaul driver...");

#ifdef MBED_CONF_APP_SLIP_SERIAL_BAUD_RATE
    slipdrv_id = pslipmacdriver->Slip_Init(mac, MBED_CONF_APP_SLIP_SERIAL_BAUD_RATE);
#else
    tr_warning("baud rate for slip not defined");
#endif

    if (slipdrv_id >= 0) {
        backhaul_driver_status_cb(1, slipdrv_id);
        return;
    }

    tr_error("Backhaul driver init failed, retval = %d", slipdrv_id);
#elif MBED_CONF_APP_BACKHAUL_DRIVER == ETH
    tr_info("Using ETH backhaul driver...");
    arm_eth_phy_device_register(mac, backhaul_driver_status_cb);
    return;
#else
#error "Unsupported backhaul driver"
#endif

}

static void mesh_system_heap_error_handler(heap_fail_t event)
{
    //tr_error("Heap error, mesh_system_heap_error_handler() %d", event);
    switch (event) {
        case NS_DYN_MEM_NULL_FREE:
        case NS_DYN_MEM_DOUBLE_FREE:
        case NS_DYN_MEM_ALLOCATE_SIZE_NOT_VALID:
        case NS_DYN_MEM_POINTER_NOT_VALID:
        case NS_DYN_MEM_HEAP_SECTOR_CORRUPTED:
        case NS_DYN_MEM_HEAP_SECTOR_UNITIALIZED:
            break;
        default:
            break;
    }
    while (1);
}

