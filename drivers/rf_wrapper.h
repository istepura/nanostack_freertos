/*
 * Copyright (c) 2016 ARM Limited. All rights reserved.
 */

#include <stdint.h>
#include "FreeRTOS.h"
#include "task.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef enum {
    StackRoleRouter,
    StackRoleBorderRouter
}StackRole_t;

int8_t start_mesh(TaskHandle_t taskHandle, StackRole_t role);
int8_t rf_device_register(void);
void rf_read_mac_address(uint8_t *mac);
#ifdef __cplusplus
}
#endif
