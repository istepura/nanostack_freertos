#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"

static SemaphoreHandle_t critical_mutex_id;
static uint8_t sys_irq_disable_counter;

void platform_critical_init(void)
{
    critical_mutex_id = xSemaphoreCreateRecursiveMutex();
    configASSERT(critical_mutex_id);
}

void platform_enter_critical(void)
{
    xSemaphoreTakeRecursive(critical_mutex_id, portMAX_DELAY);
    sys_irq_disable_counter++;
}

void platform_exit_critical(void)
{
    --sys_irq_disable_counter;
    xSemaphoreGiveRecursive(critical_mutex_id);
}
