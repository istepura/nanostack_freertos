#include "FreeRTOS.h"
#include  "task.h"
#include "semphr.h"
#include "eventOS_scheduler.h"
#include "ns_task.h"

#define CONF_NANOSTACK_HAL_EVENT_LOOP_THREAD_STACK_SIZE 5000
static SemaphoreHandle_t event_mutex_id;
static TaskHandle_t event_thread_id;
static TaskHandle_t event_mutex_owner_id = NULL;
static uint32_t owner_count = 0;

static void event_loop_thread(void *arg);

void eventOS_scheduler_signal(void)
{
    xTaskNotify(event_thread_id, 1, eSetValueWithOverwrite);
}

void eventOS_scheduler_mutex_wait(void)
{
    xSemaphoreTakeRecursive(event_mutex_id, portMAX_DELAY);
    if (0 == owner_count) {
        event_mutex_owner_id = xTaskGetCurrentTaskHandle();
    }
    owner_count++;
}

void eventOS_scheduler_idle(void)
{
    uint32_t notify_val = 0;
    //tr_debug("idle");
    eventOS_scheduler_mutex_release();
    xTaskNotifyWait(0, 0xffffffff, &notify_val, portMAX_DELAY);
    eventOS_scheduler_mutex_wait();
}

uint32_t eventOS_scheduler_sleep(uint32_t sleep_time_ms)
{
    vTaskDelay(sleep_time_ms / portTICK_PERIOD_MS);
    return sleep_time_ms;
}

uint8_t eventOS_scheduler_mutex_is_owner(void)
{
    return xTaskGetCurrentTaskHandle() == event_mutex_owner_id ? 1 : 0;
}

void eventOS_scheduler_mutex_release(void)
{
    owner_count--;
    if (0 == owner_count) {
        event_mutex_owner_id = NULL;
    }
    xSemaphoreGiveRecursive(event_mutex_id);
}

void ns_event_loop_thread_create(void)
{
    event_mutex_id = xSemaphoreCreateRecursiveMutex();
    configASSERT(event_mutex_id != NULL);

    xTaskCreate( event_loop_thread,
                    "ns_event_loop_thread", 
                    CONF_NANOSTACK_HAL_EVENT_LOOP_THREAD_STACK_SIZE /2 ,      /* Stack size in words, not bytes. */
                    ( void * ) NULL, tskIDLE_PRIORITY + 2, &event_thread_id );

    configASSERT(event_thread_id != NULL);
}

void ns_event_loop_thread_start(void)
{
}

static void event_loop_thread(void *arg)
{
    (void)arg;
    eventOS_scheduler_mutex_wait();
    eventOS_scheduler_run(); //Does not return
}
