/*
 * Copyright (c) 2016 ARM Limited. All rights reserved.
 */


#ifndef STATIC_6LOWPAN_CONFIG
#define STATIC_6LOWPAN_CONFIG

#include "cfg_parser.h"


#define MBED_CONF_APP_SECURITY_MODE  NONE
#define MBED_CONF_APP_PANA_MODE
#define MBED_CONF_APP_PSK_KEY {0xa0, 0xa1, 0xa2, 0xa3, 0xa4, 0xa5, 0xa6, 0xa7, 0xa8, 0xa9, 0xaa, 0xab, 0xac, 0xad, 0xae, 0xaf}
#define MBED_CONF_APP_PSK_KEY_ID 1
#define MBED_CONF_APP_PAN_ID 0x0691
#define MBED_CONF_APP_NETWORK_ID network000000000
#define MBED_CONF_APP_TLS_PSK_KEY {0xcf, 0xce, 0xcd, 0xcc, 0xcb, 0xca, 0xc9, 0xc8, 0xc7, 0xc6, 0xc5, 0xc4, 0xc3, 0xc2, 0xc1, 0xc0}
#define MBED_CONF_APP_RF_CHANNEL_MASK 0x07fff800
#define MBED_CONF_APP_RF_CHANNEL_PAGE 2
#define MBED_CONF_APP_RF_CHANNEL 1
#define MBED_CONF_APP_RPL_INSTANCE_ID                                         1                                                                                                // set by application
#define MBED_CONF_APP_BACKHAUL_PREFIX                                         fd00:db8:ff1::
#define MBED_CONF_APP_PREFIX_FROM_BACKHAUL                                    1 
#define MBED_CONF_APP_BACKHAUL_DYNAMIC_BOOTSTRAP                              0
#define MBED_CONF_APP_SHORT_MAC_ADDRESS                                       0xface
#define MBED_CONF_APP_TLS_PSK_KEY_ID 1
#define MBED_CONF_APP_BEACON_PROTOCOL_ID  4
#define MBED_CONF_APP_RA_ROUTER_LIFETIME 1024
#define MBED_CONF_APP_RPL_IMIN                                                12
#define MBED_CONF_APP_RPL_LIFETIME_UNIT                                       60
#define MBED_CONF_APP_RPL_MAX_RANK_INC                                        2048
#define MBED_CONF_MBED_MESH_API_6LOWPAN_ND_PANID_FILTER                       0xffff
#define MBED_CONF_APP_RPL_OCP                                                 1
#define MBED_CONF_APP_RPL_K                                                   10                                                                                              
#define MBED_CONF_APP_DEFINED_BR_CONFIG                                       true 
#define MBED_CONF_APP_RPL_DEFAULT_LIFETIME                                    64
#define MBED_CONF_APP_RPL_MIN_HOP_RANK_INC                                    128
#define MBED_CONF_APP_RPL_IDOUBLINGS                                          9 
#define MBED_CONF_APP_RA_ROUTER_LIFETIME                                      1024                                                                                             // set by application
#define MBED_CONF_APP_BACKHAUL_DRIVER                                         ETH
#define MBED_CONF_APP_RPL_PCS                                                 1 
#define MBED_CONF_APP_MULTICAST_ADDR                                          ff05::7   
#define MBED_CONF_APP_BACKHAUL_DEFAULT_ROUTE                                  ::/0

static const char psk_key[16] = MBED_CONF_APP_PSK_KEY;
static const char tls_psk_key[16] = MBED_CONF_APP_TLS_PSK_KEY;

static conf_t mbed_config[] = {
    /* NAME, STRING_VALUE, INT_VALUE */
    {"SECURITY_MODE", STR(MBED_CONF_APP_SECURITY_MODE), 0},
    {"PANA_MODE", STR(MBED_CONF_APP_PANA_MODE), 0},
    {"PSK_KEY", psk_key, 0},
    {"PSK_KEY_ID", NULL, MBED_CONF_APP_PSK_KEY_ID},
    {"PAN_ID", NULL, MBED_CONF_APP_PAN_ID},
    {"NETWORK_ID", STR(MBED_CONF_APP_NETWORK_ID), 0},
    {"PREFIX", STR(MBED_CONF_APP_PREFIX), 0},
    {"BACKHAUL_PREFIX", STR(MBED_CONF_APP_BACKHAUL_PREFIX), 0},
    {"BACKHAUL_DEFAULT_ROUTE", STR(MBED_CONF_APP_BACKHAUL_DEFAULT_ROUTE), 0},
    {"BACKHAUL_NEXT_HOP", STR(MBED_CONF_APP_BACKHAUL_NEXT_HOP), 0},
    {"RF_CHANNEL", NULL, MBED_CONF_APP_RF_CHANNEL},
    {"RF_CHANNEL_PAGE", NULL, MBED_CONF_APP_RF_CHANNEL_PAGE},
    {"RF_CHANNEL_MASK", NULL, MBED_CONF_APP_RF_CHANNEL_MASK},
    {"RPL_INSTANCE_ID", NULL, MBED_CONF_APP_RPL_INSTANCE_ID},
    {"RPL_IDOUBLINGS", NULL, MBED_CONF_APP_RPL_IDOUBLINGS},
    {"RPL_K", NULL, MBED_CONF_APP_RPL_K},
    {"RPL_MAX_RANK_INC", NULL, MBED_CONF_APP_RPL_MAX_RANK_INC},
    {"RPL_MIN_HOP_RANK_INC", NULL, MBED_CONF_APP_RPL_MIN_HOP_RANK_INC},
    {"RPL_IMIN", NULL, MBED_CONF_APP_RPL_IMIN},
    {"RPL_DEFAULT_LIFETIME", NULL, MBED_CONF_APP_RPL_DEFAULT_LIFETIME},
    {"RPL_LIFETIME_UNIT", NULL, MBED_CONF_APP_RPL_LIFETIME_UNIT},
    {"RPL_PCS", NULL, MBED_CONF_APP_RPL_PCS},
    {"RPL_OCP", NULL, MBED_CONF_APP_RPL_OCP},
    {"RA_ROUTER_LIFETIME", NULL, MBED_CONF_APP_RA_ROUTER_LIFETIME},
    {"BEACON_PROTOCOL_ID", NULL, MBED_CONF_APP_BEACON_PROTOCOL_ID},
    {"TLS_PSK_KEY", tls_psk_key, 0},
    {"TLS_PSK_KEY_ID", NULL, MBED_CONF_APP_TLS_PSK_KEY_ID},
    {"BACKHAUL_DYNAMIC_BOOTSTRAP", NULL, MBED_CONF_APP_BACKHAUL_DYNAMIC_BOOTSTRAP},
    {"SHORT_MAC_ADDRESS", NULL, MBED_CONF_APP_SHORT_MAC_ADDRESS},
    {"MULTICAST_ADDR", STR(MBED_CONF_APP_MULTICAST_ADDR), 0},
    {"PREFIX_FROM_BACKHAUL", NULL, MBED_CONF_APP_PREFIX_FROM_BACKHAUL},
    /* Array must end on {NULL, NULL, 0} field */
    {NULL, NULL, 0}
};
conf_t *global_config = mbed_config;

#endif //STATIC_6LOWPAN_CONFIG
