#include "platform/mbed_critical.h"
#include "platform/mbed_assert.h"
#include <stdbool.h>
#include <stdint.h>
#include "fsl_device_registers.h"
#include "FreeRTOS.h"
static volatile uint32_t interrupt_enable_counter = 0;
static volatile bool critical_interrupts_disabled = false;

bool core_util_are_interrupts_enabled(void)
{
#if defined(__CORTEX_A9)
    return ((__get_CPSR() & 0x80) == 0);
#else
    return ((__get_PRIMASK() & 0x1) == 0);
#endif
}

void core_util_critical_section_enter(void)
{
    portENTER_CRITICAL();
}

void core_util_critical_section_exit(void)
{
    portEXIT_CRITICAL();

}


