#include <stdint.h>
#include "platform/arm_hal_timer.h"
#include "drivers/Timer.h"
#include "drivers/Timeout.h"
#include "FreeRTOS.h"
#include "task.h"


static uint32_t due;
static void (*arm_hal_callback)(void);
static mbed::Timer timer;
static mbed::Timeout timeout;
static TaskHandle_t  timer_thread_id;


static void timer_thread(void *arg)
{
    (void)arg;
    for (;;) {
        ulTaskNotifyTake(pdTRUE, portMAX_DELAY);
        arm_hal_callback();
    }
}

void platform_timer_set_cb(platform_timer_cb new_fp)
{
    arm_hal_callback = new_fp;
}

void platform_timer_enable(void)
{
    xTaskCreate( timer_thread,
            "timer_thread", 
            2048 /2 ,      /* Stack size in words, not bytes. */
            ( void * ) NULL, configMAX_PRIORITIES - 1, &timer_thread_id );

    timer.start();
}

static void timer_callback(void)
{
    due = 0;
    BaseType_t xHigherPriorityTaskWoken = pdFALSE;
    vTaskNotifyGiveFromISR(timer_thread_id, &xHigherPriorityTaskWoken);
    portYIELD_FROM_ISR( xHigherPriorityTaskWoken );
}

void platform_timer_start(uint16_t slots)
{
    timer.reset();
    due = slots * UINT32_C(50);
    timeout.attach_us(timer_callback, due);    
}

void platform_timer_disable(void)
{
    timeout.detach();
}

uint16_t platform_timer_get_remaining_slots(void)
{
    uint32_t elapsed = timer.read_us();
    if (elapsed < due) {
        return (uint16_t) ((due - elapsed) / 50);
    } else {
        return 0;
    }
}
