#ifndef _NS_TASK_H_DEFINED__
#define _NS_TASK_H_DEFINED__

#ifdef __cplusplus
extern "C" {
#endif

void ns_event_loop_thread_create(void);
void ns_event_loop_thread_start(void);

#ifdef __cplusplus
}
#endif
#endif
