#include "fsl_debug_console.h"
#include "board.h"
#include "fsl_port.h"
#include "fsl_smc.h"
#include "fsl_dac.h"
#include "pin_mux.h"
#include "clock_config.h"
#include "FreeRTOS.h"
#include "task.h"
#include "net_interface.h"
#include "ns_task.h"
#include "eventOS_scheduler.h"
#include "eventOS_event.h"
#include "nsdynmemLIB.h"
#include "fsl_smc.h"
#include "fsl_uart.h"
#include "rf_wrapper.h"
#include "mbed-trace/mbed_trace.h"
#include "socket_api.h"
#define TRACE_GROUP "main_task"


/*******************************************************************************
 * Definitions
 ******************************************************************************/

/*******************************************************************************
 * Prototypes
 ******************************************************************************/

/*******************************************************************************
 * Variables
 ******************************************************************************/

/*******************************************************************************
 * Code
 ******************************************************************************/

/*!
 * @brief Main function
 */
#define APP_TASK_STACK_SIZE 1024
//fd00:db8:ff1:0:fec2:3d00:3:8dbf
static const ns_address_t node_address = {
        .type = ADDRESS_IPV6,
        .address = {0xfd, 0x00, 0xd, 0xb8, 0xf, 0xf1, 0x0, 0x0, 0xfe, 0xc2, 0x3d, 0x00, 0x0, 0x3, 0x8d,  0xbf},
        .identifier = 1234
    };

static void socket_callback(void* cb)
{
    socket_callback_t* scb = (socket_callback_t*)cb;
}
static void main_thread(void *arg)
{
    start_mesh(xTaskGetCurrentTaskHandle(), StackRoleBorderRouter);
    ulTaskNotifyTake(pdTRUE, portMAX_DELAY);

    int8_t socket_id = socket_open(SOCKET_UDP, 0, &socket_callback);
    configASSERT(socket_id >= 0);

    /* Note that vTaskDelay won't work if systick-based tickles idle
     * is used together with VPLS APP_Sleep. Since systick is disabled in VLPS, 
     * FreeRTOs scheduler won;t be able to maintain it's time properly
     */
    for (;;) {
        int8_t res = socket_sendto(socket_id, &node_address, "on", 3);
        configASSERT(res >= 0);
        tr_info("sent 'ON'");
        vTaskDelay( 5000 /portTICK_PERIOD_MS);

        res = socket_sendto(socket_id, &node_address, "off", 4);
        configASSERT(res >= 0);
        tr_info("sent 'OFF'");
        vTaskDelay( 5000 /portTICK_PERIOD_MS);
    }
}

void trace_printer(const char* str) {
    PRINTF("%s\n", str);
}

int main(void)
{
    //BOARD_BootClockRUN(); <-- This is done in us_ticker_init()
    BOARD_InitPins();
    BOARD_InitDebugConsole();
    SEGGER_SYSVIEW_Conf();
    SEGGER_RTT_Init();

    PRINTF("Startup\n\r");
    SMC_SetPowerModeProtection(SMC, kSMC_AllowPowerModeAll);
    mbed_trace_init();
    mbed_trace_config_set( TRACE_ACTIVE_LEVEL_INFO | TRACE_CARRIAGE_RETURN);
    mbed_trace_print_function_set(trace_printer);

    xTaskCreate( main_thread,
            "main_thread", 
            APP_TASK_STACK_SIZE /2 ,      /* Stack size in words, not bytes. */
            ( void * ) NULL, configMAX_PRIORITIES - 2, NULL);     
    vTaskStartScheduler();
    /* Should never reach this point. */
    while (true);
}

#if configUSE_TICKLESS_IDLE == 1
void APP_Sleep()
{
    DbgConsole_Deinit();
   PORT_SetPinMux(PORTB, 16, kPORT_PinDisabledOrAnalog);
    //SMC_PreEnterStopModes();
    SMC_SetPowerModeVlps(SMC);
    //SMC_SetPowerModeStop(SMC, kSMC_PartialStop2);
    //SMC_PostExitStopModes();
}    

void APP_PostSleep()
{
    /* Wait for PLL lock. */
    while (!(kMCG_Pll0LockFlag & CLOCK_GetStatusFlags()))
    {
    }
    CLOCK_SetPeeMode();     
    PORT_SetPinMux(PORTB, 16, kPORT_MuxAlt3);
    BOARD_InitDebugConsole();
}
#endif
void vApplicationStackOverflowHook( TaskHandle_t xTask,
                                    signed char *pcTaskName )
{
    configASSERT(0);
}
